var PEGJS = require( "pegjs" ),
    fs    = require( "fs" ),
    path  = require( "path" );

var grammar = fs.readFileSync( path.join( __dirname, "grammar", "comet-grammar" ), "utf8" );
var parser = PEGJS.buildParser( grammar );

var cometParser = {};

cometParser.parse = function( xml ){
  var cleaned = xml.replace( / contenteditable\="false"/g, '')
  var parsed;
  try {
    parsed = parser.parse( cleaned );
  } catch(e){
    parsed = "ERROR PARSING";
  }
  return parsed;
}

module.exports = cometParser;
