/************************************************************************
**
**  GRAMMAR FOR PARSING COMET CKEDITOR LOGIC AND RETURNING JSONLOGIC
**
************************************************************************/
{
  function makeNumber(o){
    var n = o[0].join("");
    if( o[1] !== null ){
      n = n + o[1];
    }
    return n*1;
  }
  function makeNode(key, val ){
    var node = {};
    node[key] = val;
    return node
  }
}

start
  = logicalnode

addOpers
  = '<arithmeticoperator>' [\ ]* operator:("+"/" - ") [\ ]* "</arithmeticoperator>" { return operator }

multOpers
  = '<arithmeticoperator>' [\ ]* operator:("*"/"/") [\ ]* "</arithmeticoperator>" { return operator }

logicOpers
  = operator:(logicalAnd/logicalOr) { return operator }

logicalAnd
  = '<logicaloperator>' [\ ]* operator:"AND" [\ ]* "</logicaloperator>" { return "and" }

logicalOr
  = '<logicaloperator>' [\ ]* operator:"OR" [\ ]* "</logicaloperator>" { return "or" }

dualOperators
  = '<stringoperator>' [\ ]* operator:(isEqualTo/isNotEqualTo/isGreaterThanEqualTo/isGreaterThan/isLessThanEqualTo/isLessThan/matches/startsWith) [\ ]* "</stringoperator>" { return operator }

singleOperators
  = '<stringoperator>' [\ ]* operator:(isBlank/isNotBlank/isPresent/isNotPresent/isSelected) [\ ]* "</stringoperator>" { return operator }
priorOperators
  = '<stringoperator>' [\ ]* operator:(isModified/isNotModified) [\ ]* "</stringoperator>" { return operator }

isBlank
 = "is blank" { return "isBlank" }

isNotBlank
 = "is not blank" { return "isNotBlank"; }

isEqualTo
 = "is equal to" { return "=="; }

isNotEqualTo
 = "is not equal to " { return "!="; }

isGreaterThan
 = "is greater than" { return ">"; }

isGreaterThanEqualTo
 = "is greater than or equal to" { return ">="; }

isLessThan
 = "is less than" { return "<"; }

isLessThanEqualTo
 = "is less than or equal to" { return "<="; }

matches
 = "matches" { return "matches"; }

isModified
 = "is modified" { return "!="; }

isNotModified
 = "is not modified" { return "==";}

isPresent
 = "is present" { return "isPresent" }

isNotPresent
 = "is not present" { return "isNotPresent" }

isSelected
 = "is selected" { return "isSelected" }

startsWith
 = "starts with" { return "startsWith"}

logicalnode
  =( left:dual operator:logicOpers right:logicalnode ) { return makeNode(operator, [ left, right ] ) }
  /dual/single/prior

dual
  =( left:additive operator:dualOperators right:additive ) { return makeNode(operator, [ left, right ] ) }
  /single/prior

single
  =( left:variable operator:singleOperators ){ return makeNode( operator, [ left ] )}
  /prior
  /additive
prior
  =( left:variable operator:priorOperators ){ return makeNode( operator, [ left, { var: "prior." + left.var.replace(/^model\./, '') }])}
  /additive
additive
  =( left:multiplicative operator:addOpers right:additive ) {return makeNode(operator, [ left, right] )}
  /multiplicative

multiplicative
  =( left:primary operator:multOpers right:multiplicative ) { return makeNode(operator, [ left, right]) }
  / primary

primary
  = number/variable/text
  /( '<bracket>(</bracket>' logicalnode:logicalnode '<bracket>)</bracket>' ) { return logicalnode; }

text "text"
  =( '<textblock>' [\ ]* ['"]  string:([0-9a-zA-Z]+) ['"] [\ ]* "</textblock>" ) { return string.join(""); }

number "number"
  =( '<textblock>' [\ ]* numbers:([0-9]+decimal?) [\ ]* "</textblock>" ) { return makeNumber(numbers); }

decimal "decimal"
  = point:"." precision:[0-9]+ {return point + precision.join(""); }

variable "variable"
  =( '<dataelement>' [\ ]* chars:[a-zA-Z\._0-9]+ [\ ]* "</dataelement>") { return { var: "model." + chars.join("")}; }
