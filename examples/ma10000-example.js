var   fs = require( "fs" ),
    path = require( "path" ),
  parser = require( '../parser.js' );

var exps = JSON.parse( fs.readFileSync( path.join( __dirname, "..", "docs", "ma-logics.json" ), "utf8" ) );

exps.forEach(function(exp, index){
  var parsed;
  try {
    parsed = parser.parse( exp.xml );
  } catch(e){
    parsed = "ERROR PARSING";
  }
  
  exps[index].jsonlogic = parsed
  if( index == exps.length-1 ){
    done();
  }
})
function done(){
fs.writeFileSync( "ma-logic-completed.json", JSON.stringify( exps, null, 2 ))
}
