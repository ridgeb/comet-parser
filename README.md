# comet-parser

Parser using [pegjs](https://github.com/pegjs/pegjs) to validate COMET ckeditor saved logic strings and produce the resulting [jsonlogic](http://jsonlogic.com/)

Grammar file: [comet-grammar](grammar/comet-grammar)

Example:
```javascript
var parser = require( './parser.js' );

// A sample valid syntax XML
var cometXML = '<dataelement contenteditable="false">TOTAL_COSTS</dataelement><stringoperator contenteditable="false">is greater than</stringoperator><textblock contenteditable="false">100</textblock>';

console.log( parser.parse( cometXML ) ); // { '>': [ { var: 'TOTAL_COSTS' }, 100 ] }

// A sample invalid syntax XML
cometXML = '<bracket>(</bracket><dataelement>RENT_PAY_TOT_NONE</dataelement><logicaloperator>AND</logicaloperator><dataelement>RENT_PAY_TOT</dataelement><bracket>)</bracket><stringoperator>is blank</stringoperator>';

console.log( parser.parse( cometXML ) ); // ERROR PARSING
```
